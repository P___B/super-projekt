﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Super_projekt___Baranowski_Patryk_8515_Z613.Startup))]
namespace Super_projekt___Baranowski_Patryk_8515_Z613
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
