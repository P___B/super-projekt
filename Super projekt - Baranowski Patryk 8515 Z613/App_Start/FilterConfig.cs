﻿using System.Web;
using System.Web.Mvc;

namespace Super_projekt___Baranowski_Patryk_8515_Z613
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
